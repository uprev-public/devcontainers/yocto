ARG BASE_IMAGE=uprev/base
ARG BASE_TAG=ubuntu-20.04
FROM ${BASE_IMAGE}:${BASE_TAG}


RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \ 
    gawk wget git-core diffstat unzip texinfo gcc-multilib \
    build-essential chrpath socat cpio python3 python3-pip python3-pexpect python-is-python3 \
    xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
    pylint3 xterm bsdmainutils libgmp-dev libmpc-dev libssl-dev \
    bsdmainutils libgmp-dev libmpc-dev libssl-dev lz4 zstd \
    && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* 



# Additional host packages required by poky/scripts/wic
RUN apt-get install -y curl dosfstools mtools parted syslinux tree zip

# Add "repo" tool (used by many Yocto-based projects)
RUN curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo


# Fix error "Please use a locale setting which supports utf-8."
# See https://wiki.yoctoproject.org/wiki/TipsAndTricks/ResolvingLocaleIssues
RUN apt-get install -y locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
        echo 'LANG="en_US.UTF-8"'>/etc/default/locale && \
        dpkg-reconfigure --frontend=noninteractive locales && \
        update-locale LANG=en_US.UTF-8

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

USER dev
WORKDIR /home/dev
CMD "/bin/bash"